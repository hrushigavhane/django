pipeline {
    agent any

    environment {
        AWS_REGION = 'eu-north-1'  // AWS region
        AWS_ACCOUNT_ID = '703671897854'  // AWS Account ID
        REPOSITORY_NAME = 'my-app'  // ECR repository name
        IMAGE_TAG = 'latest'  // Docker image tag (can be updated for different versions)
        ECR_URI = "${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com"  // ECR URI for the repository
        CFT_DIR = 'CFT'  // Directory containing CloudFormation templates
        S3_BUCKET = 'my-cloudformation-bucket11111'  // S3 bucket for storing CloudFormation templates
        PARENT_STACK = 'parent-stack'  // Name for the main stack
    }

    stages {
        stage('Clone Repository') {
            steps {
                // Clone the Git repository containing the application code
                git credentialsId: 'gitlab-credentials', branch: 'main', url: 'https://gitlab.com/hrushigavhane/django.git'
            }
        }

        stage('Upload CloudFormation Templates to S3') {
            steps {
                withAWS(credentials: 'aws-credentials', region: AWS_REGION) {
                    // Sync all files in the CloudFormation templates directory to the S3 bucket
                    sh "aws s3 sync ${CFT_DIR} s3://${S3_BUCKET}/cloudformation --delete"
                }
            }
        }

        stage('Login to AWS ECR') {
            steps {
                withAWS(credentials: 'aws-credentials', region: AWS_REGION) {
                    // Log in to AWS ECR to push images to the repository
                    sh "aws ecr get-login-password --region $AWS_REGION | docker login --username AWS --password-stdin $ECR_URI"
                }
            }
        }

        stage('Build and Push Docker Image') {
            steps {
                // Build the Docker image and push it to ECR
                sh """
                    docker build --no-cache -t $REPOSITORY_NAME:$IMAGE_TAG .  
                    docker tag $REPOSITORY_NAME:$IMAGE_TAG $ECR_URI/$REPOSITORY_NAME:$IMAGE_TAG  
                    docker push $ECR_URI/$REPOSITORY_NAME:$IMAGE_TAG  
                """
            }
        }

        stage('Deploy Parent CloudFormation Stack') {
            steps {
                withAWS(credentials: 'aws-credentials', region: AWS_REGION) {
                    script {
                        echo "Deploying parent stack: ${PARENT_STACK}"

                        // Check if the stack already exists
                        def stackExists = sh(script: """
                            aws cloudformation describe-stacks --stack-name ${PARENT_STACK} --region ${AWS_REGION} --query "Stacks[0].StackStatus" --output text || echo "StackNotFound"
                        """, returnStdout: true).trim()

                        // Create or update the stack based on its existence
                        if (stackExists == "StackNotFound") {
                            echo "Stack not found. Creating new stack."
                            // Create the CloudFormation stack if it doesn't exist
                            sh """
                                aws cloudformation create-stack \
                                    --stack-name ${PARENT_STACK} \
                                    --template-url https://s3.${AWS_REGION}.amazonaws.com/${S3_BUCKET}/cloudformation/parent.yaml \
                                    --region ${AWS_REGION} \
                                    --capabilities CAPABILITY_NAMED_IAM
                            """
                        } else {
                            echo "Stack exists. Updating stack."
                            // Update the CloudFormation stack if it already exists
                            sh """
                                aws cloudformation update-stack \
                                    --stack-name ${PARENT_STACK} \
                                    --template-url https://s3.${AWS_REGION}.amazonaws.com/${S3_BUCKET}/cloudformation/parent.yaml \
                                    --region ${AWS_REGION} \
                                    --capabilities CAPABILITY_NAMED_IAM || echo "No changes needed"
                            """
                        }

                        // Wait for the stack to be fully created or updated
                        echo "Waiting for ${PARENT_STACK} to complete..."
                        sh "aws cloudformation wait stack-create-complete --stack-name ${PARENT_STACK} --region ${AWS_REGION} || aws cloudformation wait stack-update-complete --stack-name ${PARENT_STACK} --region ${AWS_REGION}"
                    }
                }
            }
        }
    }
}
