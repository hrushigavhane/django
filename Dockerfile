# Stage 1: Base image
FROM python:3.10-slim AS django_setup

# Avoid prompts during installation
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y \
    git \
    default-mysql-client \
    build-essential \
    libmariadb-dev \
    libmariadb-dev-compat \
    pkg-config \
    nginx && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Create a directory for the Django application
WORKDIR /app

# Copy requirements file
COPY requirements.txt /app/

# Upgrade pip and install Python dependencies
RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

# Install cryptography package
RUN pip install cryptography

# Install Gunicorn for production
RUN pip install gunicorn

# Clone the GitHub repository directly into the desired location
RUN git clone https://gitlab.com/hrushigavhane/django.git /app/DeployKwiqreply

# Change directory to the Django project
WORKDIR /app/DeployKwiqreply/waservice

# Run migrations
RUN python manage.py makemigrations && python manage.py migrate

# Collect static files for production
RUN python manage.py collectstatic --noinput

# Copy the Nginx configuration file into the container
COPY nginx.conf /etc/nginx/sites-available/default

# Expose port 80 for Nginx and port 8000 for Gunicorn
EXPOSE 80 8000

# Command to run both Nginx and Gunicorn
CMD service nginx start && gunicorn --workers 3 --bind 0.0.0.0:8000 waservice.wsgi:application
